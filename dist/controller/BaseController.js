'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Base controller
 */
var BaseController = function () {
    function BaseController() {
        _classCallCheck(this, BaseController);
    }

    _createClass(BaseController, [{
        key: 'validateParams',


        /**
         * Validates that the request contains the required params
         *
         * @param requiredParams
         * @param params
         */
        value: function validateParams() {
            var requiredParams = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
            var params = arguments[1];


            console.log('validating params');
            console.debug(requiredParams);
            console.debug(params);

            if (!!params && params.constructor === Object) {
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                    for (var _iterator = requiredParams[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var paramKey = _step.value;

                        if (!params.hasOwnProperty(paramKey)) {
                            throw new Error('Param ' + value + ' is required');
                        }
                    }
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }
            }

            if (!!params && params.constructor === Array) {
                var _iteratorNormalCompletion2 = true;
                var _didIteratorError2 = false;
                var _iteratorError2 = undefined;

                try {
                    for (var _iterator2 = requiredParams[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                        var _paramKey = _step2.value;

                        if (params.indexOf(_paramKey) === -1) {
                            throw new Error('Param ' + value + ' is required');
                        }
                    }
                } catch (err) {
                    _didIteratorError2 = true;
                    _iteratorError2 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion2 && _iterator2.return) {
                            _iterator2.return();
                        }
                    } finally {
                        if (_didIteratorError2) {
                            throw _iteratorError2;
                        }
                    }
                }
            }

            throw new Error('params need to be an arry or an object');
        }
    }]);

    return BaseController;
}();

exports.default = BaseController;