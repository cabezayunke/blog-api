'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _BlogPostModel = require('../model/BlogPostModel');

var _BlogPostModel2 = _interopRequireDefault(_BlogPostModel);

var _ParamValidator = require('../validator/ParamValidator');

var _ParamValidator2 = _interopRequireDefault(_ParamValidator);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Blog post controller
 * (will be used by the router)
 */
var BlogPostController = function () {
    function BlogPostController() {
        _classCallCheck(this, BlogPostController);
    }

    _createClass(BlogPostController, [{
        key: 'getBlogPosts',


        /**
         * Get blog posts
         *
         * @param req
         * @param res
         * @param next
         * @returns {Promise.<void>}
         */
        value: function getBlogPosts(req, res, next) {
            // set default pagination to get the first 10 items sorted by ID DESC
            var options = {
                limit: req.query.hasOwnProperty('limit') ? parseInt(req.query.limit, 10) : 10,
                offset: req.query.hasOwnProperty('offset') ? parseInt(req.query.offset, 0) : 0,
                sort: {
                    _id: req.query.hasOwnProperty('order') ? req.query.order : 'desc'
                }
            };

            // paginate already uses bluebird promises
            _BlogPostModel2.default.paginate({}, options).then(function (result) {
                res.json(result);
            }).catch(function (err) {
                next(err);
            });
        }
    }, {
        key: 'getBlogPostById',


        /**
         * Get blog post by ID
         *
         * @param req
         * @param res
         * @param next
         */
        value: function getBlogPostById(req, res, next) {
            var id = req.params.id;
            var isValid = _ParamValidator2.default.validateMongoId(id);
            console.log(isValid);

            if (isValid) {
                console.log(id);

                // bluebird promise to return entity by ID
                _BlogPostModel2.default.findById(id).then(function (data) {
                    if (data) {
                        console.log('Blog post found!');
                        res.status(_httpStatus2.default.OK).json(data);
                    } else {
                        res.sendStatus(_httpStatus2.default.NOT_FOUND);
                    }
                }).catch(function (err) {
                    console.warn(err);
                    return next(err);
                });
            } else {
                // validation error
                // to be consistent, we use the same error format here as the payload-validator
                res.status(_httpStatus2.default.BAD_REQUEST).json(_ParamValidator2.default.getValidationError('_id'));
            }
        }
    }, {
        key: 'createNewBlogPost',


        /**
         * Creates new blog post
         *
         * @param req
         * @param res
         * @param next
         * @returns {Promise.<void>}
         */
        value: function createNewBlogPost(req, res, next) {

            var isValid = _ParamValidator2.default.validateBody(req.body, // data
            ['title', 'createdBy', 'body'] // mandatory params
            );

            if (isValid.success) {
                var newBlogPost = new _BlogPostModel2.default(req.body);
                // bluebird promise to return saved entity
                newBlogPost.save().then(function () {
                    console.log('New blog post created');
                    res.status(_httpStatus2.default.CREATED).json(newBlogPost);
                }).catch(function (err) {
                    console.warn(err);
                    return next(err);
                });
            } else {
                res.status(_httpStatus2.default.BAD_REQUEST).json(isValid.response);
            }
        }
    }, {
        key: 'deleteBlogPost',


        /**
         * Delete blog post
         *
         * @param req
         * @param res
         * @param next
         */
        value: function deleteBlogPost(req, res, next) {

            var id = req.params.id;
            var isValid = _ParamValidator2.default.validateMongoId(id);
            console.log(isValid);

            if (isValid) {
                console.log(id);

                // bluebird promise to return entity by ID
                _BlogPostModel2.default.findById(id).exec().then(function (data) {
                    console.log(data);
                    if (data) {
                        console.log('Blog post to delete found');
                        // bluebird promise to delete blog post
                        return data.remove();
                    } else {
                        res.sendStatus(_httpStatus2.default.NOT_FOUND);
                    }
                }).then(function (data) {
                    if (data) {
                        console.log('Blog post successfully deleted!');
                        res.status(_httpStatus2.default.OK).json(data);
                    }
                }).catch(function (err) {
                    console.warn(err);
                    return next(err);
                });
            } else {
                // validation error
                // to be consistent, we use the same error format here as the payload-validator
                res.status(_httpStatus2.default.BAD_REQUEST).json(_ParamValidator2.default.getValidationError('_id'));
            }
        }

        /**
         * Updates a blog post
         *
         * @param req
         * @param res
         * @param next
         * @returns {Promise.<void>}
         */

    }, {
        key: 'updateBlogPost',
        value: function updateBlogPost(req, res, next) {

            var id = req.params.id;
            var isValidId = _ParamValidator2.default.validateMongoId(id);

            var isValidBody = _ParamValidator2.default.validateBody(req.body, // data
            [] // mandatory params
            );

            if (isValidId) {
                if (isValidBody.success) {
                    // bluebird promise to return updated entity
                    _BlogPostModel2.default.findByIdAndUpdate(id, { $set: req.body }, { new: true }).then(function (data) {
                        if (data) {
                            console.log('Blog post updated');
                            res.status(_httpStatus2.default.OK).json(data);
                        } else {
                            res.sendStatus(_httpStatus2.default.NOT_FOUND);
                        }
                    }).catch(function (err) {
                        console.warn(err);
                        return next(err);
                    });
                } else {
                    res.status(_httpStatus2.default.BAD_REQUEST).json(isValidBody.response);
                }
            } else {
                // validation error
                // to be consistent, we use the same error format here as the payload-validator
                res.status(_httpStatus2.default.BAD_REQUEST).json(_ParamValidator2.default.getValidationError('_id'));
            }
        }
    }]);

    return BlogPostController;
}();

exports.default = BlogPostController;