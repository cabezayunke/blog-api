'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _BlogPostController = require('./controller/BlogPostController');

var _BlogPostController2 = _interopRequireDefault(_BlogPostController);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = new _express.Router();

/*
 * Posts routes
 */
var controller = new _BlogPostController2.default();
router.get('/blog/posts/:id', controller.getBlogPostById);
router.get('/blog/posts', controller.getBlogPosts);
router.post('/blog/posts', controller.createNewBlogPost);
router.delete('/blog/posts/:id', controller.deleteBlogPost);
router.put('/blog/posts/:id', controller.updateBlogPost);

/*
 * Custom routes
 */

/*
 * END of custom routes
 */

exports.default = router;