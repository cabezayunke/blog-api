'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _payloadValidator = require('payload-validator');

var _payloadValidator2 = _interopRequireDefault(_payloadValidator);

var _validator = require('validator');

var _validator2 = _interopRequireDefault(_validator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Helper class to validate CRUD Controllers
 */
var ParamValidator = function () {
    function ParamValidator() {
        _classCallCheck(this, ParamValidator);
    }

    _createClass(ParamValidator, null, [{
        key: 'getValidationError',


        /**
         * Get validation error in the same format as the payload-validator error
         * @returns {{errorMessage: string, errorKey: [string]}}
         */
        value: function getValidationError(field) {
            return {
                "errorMessage": "Parameter '" + field + "' value passed is not valid",
                "errorKey": [field]
            };
        }

        /**
         * Validates the ID recevied is a valid MongoID
         * (to use in multiple requests)
         * @param id
         */

    }, {
        key: 'validateMongoId',
        value: function validateMongoId(id) {
            return _validator2.default.isMongoId(id);
        }

        /**
         * Validates that the expected body received is what we expect
         *
         * @param data
         * @param mandatoryParams
         * @returns {*}
         */

    }, {
        key: 'validateBody',
        value: function validateBody(data, mandatoryParams) {

            for (var key in data) {
                if (!data.hasOwnProperty(key)) continue;
                if (data[key] === '_id' || data[key] === '__v') continue;

                if (data[key] === 'undefined' || data[key] === null || data[key].length === 0) {
                    // to be consistent, we return the same format as the payload-validator
                    return {
                        success: false,
                        response: this.getValidationError(key)
                    };
                }
            }

            return _payloadValidator2.default.validator(data, {
                "title": "",
                "body": "",
                "createdBy": {
                    "userId": "",
                    "name": "",
                    "photoUrl": ""
                }
            }, mandatoryParams, true);
        }

        /*
         * Custom validation below
         */

        /*
         * END of custom validation
         */

    }]);

    return ParamValidator;
}();

exports.default = ParamValidator;