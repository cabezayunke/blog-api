import BlogPostModel from '../model/BlogPostModel';
import ParamValidator from '../validator/ParamValidator';
import HttpStatus from 'http-status';
import Promise from 'bluebird';

/**
 * Blog post controller
 * (will be used by the router)
 */
export default class BlogPostController {

    /**
     * Get blog posts
     *
     * @param req
     * @param res
     * @param next
     */
    getBlogPosts(req, res, next) {
        // set default pagination to get the first 10 items sorted by ID DESC
        let options = {
            limit:  req.query.hasOwnProperty('limit') ? parseInt(req.query.limit, 10) : 10,
            offset: req.query.hasOwnProperty('offset') ? parseInt(req.query.offset, 0) : 0,
            sort: {
                _id: req.query.hasOwnProperty('order') ? req.query.order : 'desc'
            }
        };

        // paginate already uses bluebird promises
        BlogPostModel.paginate({}, options)
            .then((result) => {
                res.json(result);
            })
            .catch((err) => {
                next(err);
            });
    };

    /**
     * Get blog post by ID
     *
     * @param req
     * @param res
     * @param next
     */
    getBlogPostById(req, res, next) {
        let id = req.params.id;
        let isValid = ParamValidator.validateMongoId(id);
        console.log(isValid);

        if(isValid) {
            console.log(id);

            // bluebird promise to return entity by ID
            BlogPostModel.findById(id)
                .then((data) => {
                    if(data) {
                        console.log('Blog post found!');
                        res.status(HttpStatus.OK).json(data);
                    }
                    else {
                        res.sendStatus(HttpStatus.NOT_FOUND);
                    }
                })
                .catch((err) => {
                    console.warn(err);
                    return next(err);
                });

        }
        else {
            // validation error
            // to be consistent, we use the same error format here as the payload-validator
            res.status(HttpStatus.BAD_REQUEST).json(ParamValidator.getValidationError('_id'));
        }

    };

    /**
     * Creates new blog post
     *
     * @param req
     * @param res
     * @param next
     */
    createNewBlogPost(req, res, next) {

        let isValid = ParamValidator.validateBody(
            req.body, // data
            ['title', 'createdBy', 'body'] // mandatory params
        );

        if(isValid.success) {
            let newBlogPost = new BlogPostModel(req.body);
            // bluebird promise to return saved entity
            newBlogPost.save()
            .then(() => {
                console.log('New blog post created');
                res.status(HttpStatus.CREATED).json(newBlogPost);
            })
            .catch((err) => {
                console.warn(err);
                return next(err);
            });
        }
        else {
            res.status(HttpStatus.BAD_REQUEST).json(isValid.response);
        }

    };

    /**
     * Delete blog post
     *
     * @param req
     * @param res
     * @param next
     */
    deleteBlogPost(req, res, next) {

        let id = req.params.id;
        let isValid = ParamValidator.validateMongoId(id);
        console.log(isValid);

        if(isValid) {
            console.log(id);

            // bluebird promise to return entity by ID
            BlogPostModel.findById(id).exec()
                .then((data) => {
                    console.log(data);
                    if(data) {
                        console.log('Blog post to delete found');
                        // bluebird promise to delete blog post
                        return data.remove();
                    }
                    else {
                        res.sendStatus(HttpStatus.NOT_FOUND);
                    }
                })
                .then((data) => {
                    if(data) {
                        console.log('Blog post successfully deleted!');
                        res.status(HttpStatus.OK).json(data);
                    }
                })
                .catch((err) => {
                    console.warn(err);
                    return next(err);
                });

        }
        else {
            // validation error
            // to be consistent, we use the same error format here as the payload-validator
            res.status(HttpStatus.BAD_REQUEST).json(ParamValidator.getValidationError('_id'));
        }

    }


    /**
     * Updates a blog post
     *
     * @param req
     * @param res
     * @param next
     */
    updateBlogPost(req, res, next) {

        let id = req.params.id;
        let isValidId = ParamValidator.validateMongoId(id);

        let isValidBody = ParamValidator.validateBody(
            req.body, // data
            [] // mandatory params
        );

        if(isValidId) {
            if(isValidBody.success) {
                // bluebird promise to return updated entity
                BlogPostModel.findByIdAndUpdate(id, { $set: req.body }, {new: true})
                .then((data) => {
                    if(data) {
                        console.log('Blog post updated');
                        res.status(HttpStatus.OK).json(data);
                    }
                    else {
                        res.sendStatus(HttpStatus.NOT_FOUND);
                    }
                })
                .catch((err) => {
                    console.warn(err);
                    return next(err);
                });
            }
            else {
                res.status(HttpStatus.BAD_REQUEST).json(isValidBody.response);
            }
        }
        else {
            // validation error
            // to be consistent, we use the same error format here as the payload-validator
            res.status(HttpStatus.BAD_REQUEST).json(ParamValidator.getValidationError('_id'));
        }
    };

    /*
     * Custom controller methods here
     */

    /*
     * END of custom controller methods
     */
}