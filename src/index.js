import config from 'config';
import helmet from 'helmet';
import cors from 'cors';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import express from 'express';
import HttpStatus from 'http-status';
import mongoose from 'mongoose';

const env = process.env.NODE_ENV || 'production';
console.log('env = ' + env);


// create app
const app = express();
// connect to database
app.db = mongoose.connect(
    'mongodb://' + config.get('mongo.host') + ':' + config.get('mongo.port'),
    {
        useMongoClient: true
    }
);
mongoose.connection.on('error', (err) => {
    throw err;
});

// Helmet helps you secure your Express apps by setting various HTTP headers
// https://github.com/helmetjs/helmet
app.use(helmet());

// Enable CORS with various options
// https://github.com/expressjs/cors
app.use(cors());

// logging requests
app.use(morgan('dev'));

// parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


/**
 * Routes
 */
import routes from './router';
app.use('/api', routes);

//catch 404 and forward to error handler
app.use(function(req, res, next) {
    let err = new Error('Not Found');
    err.status = HttpStatus.NOT_FOUND;
    next(err);
});


// error handler
// stacktrace only in development
app.use(function(err, req, res) {
    console.error(err);
    res.status(err.status || HttpStatus.INTERNAL_SERVER_ERROR);

    let error = {};
    if (env === 'development') {
        error = err;
    }

    res.json({
        message: err.message,
        error: error
    });
});

/**
 * Start server
 */
const port = config.get('node.port') || 3000;
const gateway = config.get('node.gateway') || "127.0.0.1";

app.listen(port, gateway, () => {
    console.log('Blog API server listening to ' + gateway + ' on port ' + port);
});

module.exports = app; // for testing