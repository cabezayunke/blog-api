import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';
import Promise from 'bluebird';

// make mongoose use bluebird
mongoose.Promise = Promise;

/**
 * BlogPost schema
 */
const schema = mongoose.Schema({
    _id:  {
        type: mongoose.Schema.Types.ObjectId,
        index: true,
        required: true,
        auto: true
    },
    title: {
        type: String, required: true
    },
    createdBy: {
        userId: String,
        name: String,
        photoUrl: String
    },
    createdAt: {
        type: Date, default: Date.now
    },
    body: String
});

schema.plugin(mongoosePaginate);

// create model from schema
const BlogPostModel = mongoose.model('BlogPostModel', schema);
export default BlogPostModel;