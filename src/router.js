import { Router } from 'express';
import BlogPostController from './controller/BlogPostController';

const router = new Router();

/*
 * Posts routes
 */
const controller = new BlogPostController();
router.get('/blog/posts/:id', controller.getBlogPostById);
router.get('/blog/posts', controller.getBlogPosts);
router.post('/blog/posts', controller.createNewBlogPost);
router.delete('/blog/posts/:id', controller.deleteBlogPost);
router.put('/blog/posts/:id', controller.updateBlogPost);

/*
 * Custom routes
 */

/*
 * END of custom routes
 */

export default router;