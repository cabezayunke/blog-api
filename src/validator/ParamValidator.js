import payloadValidator from 'payload-validator';
import validator from 'validator';

/**
 * Helper class to validate CRUD Controllers
 */
class ParamValidator {

    /**
     * Get validation error in the same format as the payload-validator error
     * @returns {{errorMessage: string, errorKey: [string]}}
     */
    static getValidationError(field) {
        return {
            "errorMessage": "Parameter '" + field + "' value passed is not valid",
            "errorKey": [field]
        }
    }

    /**
     * Validates the ID recevied is a valid MongoID
     * (to use in multiple requests)
     * @param id
     */
    static validateMongoId(id) {
        return validator.isMongoId(id);
    }

    /**
     * Validates that the expected body received is what we expect
     *
     * @param data
     * @param mandatoryParams
     * @returns {*}
     */
    static validateBody(data, mandatoryParams) {

        for(let key in data) {
            if (!data.hasOwnProperty(key)) continue;
            if (data[key] === '_id' || data[key] === '__v') continue;

            if(data[key] === 'undefined' || data[key] === null || data[key].length === 0) {
                // to be consistent, we return the same format as the payload-validator
                return {
                    success: false,
                    response: this.getValidationError(key)
                }
            }
        }

        return payloadValidator.validator(
            data,
            {
                "title": "",
                "body": "",
                "createdBy": {
                    "userId": "",
                    "name": "",
                    "photoUrl": ""
                }
            },
            mandatoryParams,
            true
        );
    }

    /*
     * Custom validation below
     */

    /*
     * END of custom validation
     */
}

export default ParamValidator;