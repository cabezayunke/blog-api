//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

const HttpStatus = require('http-status');
const config = require('config');

//Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../dist/index.js');
const should = chai.should();
const cleanMongo = require('clean-mongo');

chai.use(chaiHttp);

// test data
const testdata = require('./testdata');
console.log(testdata);

describe('CRUD requests', () => {

    // clean up database
    beforeEach((done) => {
        cleanMongo('mongodb://' + config.get('mongo.host') + ':' + config.get('mongo.port'))
            .then(() => {
                done();
            });
    });

    /*
     * Test the /POST route
     */
    describe('/POST requests', () => {
        it('it should create a new entity', (done) => {
            chai.request(server)
                .post('/api/blog/posts')
                .send(testdata.entitySample)
                .end((err, res) => {
                    if(err) console.warn(err.response.res.text);

                    let postData = res.body;
                    res.should.have.status(HttpStatus.CREATED);

                    for(let key in postData) {
                        if (!postData.hasOwnProperty(key)) continue;
                        if (postData[key] === '_id' || postData[key] === '__v') continue;

                        postData[key].should.be.eql(testdata.entitySample[key]);
                    }

                    done();
            });
        });

        testdata.invalidPostData.forEach((data) => {
            it('it should return bad request', (done) => {
                chai.request(server)
                    .post('/api/blog/posts')
                    .send(data)
                    .end((err, res) => {
                        console.log(data);

                        if(err) console.warn(err.response.res.text);
                        res.should.have.status(HttpStatus.BAD_REQUEST);
                        done();
                    });
            });
        });
    });

    /*
     * Test the /PUT route
     */
    describe('/PUT requests', () => {
        it('it should update an entity', (done) => {
            chai.request(server)
                .post('/api/blog/posts')
                .send(testdata.entitySample)
                .end((err, res) => {
                    if(err) console.warn(err.response.res.text);

                    let postData = res.body;
                    console.log(postData);

                    chai.request(server)
                        .put('/api/blog/posts/' + postData._id)
                        .send(testdata.updatedEntitySample)
                        .end((err, res) => {
                            if(err) console.warn(err.response.res.text);

                            let putData = res.body;
                            console.log(putData);

                            for(let key in putData) {
                                if (!putData.hasOwnProperty(key)) continue;
                                if (putData[key] === '_id' || putData[key] === '__v') continue;

                                putData[key].should.be.eql(testdata.updatedEntitySample[key]);
                            }

                            done();
                        });
                });
        });

        testdata.invalidPutData.forEach((data) => {
            it('it should return bad request', (done) => {

                chai.request(server)
                    .post('/api/blog/posts')
                    .send(testdata.entitySample)
                    .end((err, res) => {
                        if(err) console.warn(err.response.res.text);

                        let postData = res.body;

                        chai.request(server)
                            .put('/api/blog/posts/' + postData._id)
                            .send(data)
                            .end((err, res) => {
                                if(err) console.warn(err.response.res.text);
                                res.should.have.status(HttpStatus.BAD_REQUEST);
                                done();
                            });
                    });
            });
        });

        testdata.invalidIds.forEach((data) => {
            it('it should return invalid ID', (done) => {
                chai.request(server)
                    .put('/api/blog/posts/' + data)
                    .send(testdata.entitySample)
                    .end((err, res) => {
                        if(err) console.warn(err.response.res.text);
                        res.should.have.status(HttpStatus.BAD_REQUEST);
                        done();
                    });
            });
        });

        it('it should return not found', (done) => {
            chai.request(server)
                .put('/api/blog/posts/' + testdata.entitySample._id)
                .send(testdata.entitySample)
                .end((err, res) => {
                    if(err) console.warn(err.response.res.text);
                    res.should.have.status(HttpStatus.NOT_FOUND);
                    done();
                });
        });
    });
    /*
     * Test the /GET route
     */
    describe('/GET requests', () => {
        it('it should GET first 10 entities by default (empty)', (done) => {
            chai.request(server)
                .get('/api/blog/posts')
                .end((err, res) => {
                    if(err) console.warn(err.response.res.text);
                    res.should.have.status(HttpStatus.OK);
                    res.body.docs.should.be.a('array');
                    res.body.docs.length.should.be.eql(0);
                    res.body.limit.should.be.a('number');
                    res.body.limit.should.be.eql(10);
                    res.body.total.should.be.a('number');
                    res.body.total.should.be.eql(0);
                    res.body.offset.should.be.a('number');
                    res.body.offset.should.be.eql(0);
                    done();
                });
        });

        it('it should GET next 20 entities (empty)', (done) => {
            chai.request(server)
                .get('/api/blog/posts?limit=20&offset=10')
                .end((err, res) => {
                    if(err) console.warn(err.response.res.text);
                    res.should.have.status(HttpStatus.OK);
                    res.body.docs.should.be.a('array');
                    res.body.docs.length.should.be.eql(0);
                    res.body.limit.should.be.a('number');
                    res.body.limit.should.be.eql(20);
                    res.body.total.should.be.a('number');
                    res.body.total.should.be.eql(0);
                    res.body.offset.should.be.a('number');
                    res.body.offset.should.be.eql(10);
                    done();
                });
        });

        it('it should GET first 10 entities by default (1 entity)', (done) => {
            // create blog post
            chai.request(server)
                .post('/api/blog/posts')
                .send(testdata.entitySample)
                .end((err, res) => {
                    if(err) console.warn(err.response.res.text);

                    let postData = res.body;

                    // get items
                    chai.request(server)
                        .get('/api/blog/posts')
                        .end((err, res) => {
                            if(err) console.warn(err.response.res.text);
                            res.should.have.status(HttpStatus.OK);
                            res.body.docs.should.be.a('array');
                            res.body.docs.length.should.be.eql(1);
                            res.body.limit.should.be.a('number');
                            res.body.limit.should.be.eql(10);
                            res.body.total.should.be.a('number');
                            res.body.total.should.be.eql(1);
                            res.body.offset.should.be.a('number');
                            res.body.offset.should.be.eql(0);
                            done();
                        });

                });
        });

        testdata.invalidIds.forEach((data) => {
            it('it should return invalid ID', (done) => {
                chai.request(server)
                    .get('/api/blog/posts/' + data)
                    .end((err, res) => {
                        if(err) console.warn(err.response.res.text);
                        res.should.have.status(HttpStatus.BAD_REQUEST);
                        done();
                    });
            });
        });

        it('it should return not found', (done) => {
            chai.request(server)
                .get('/api/blog/posts/' + testdata.entitySample._id)
                .end((err, res) => {
                    if(err) console.warn(err.response.res.text);
                    res.should.have.status(HttpStatus.NOT_FOUND);
                    done();
                });
        });

        it('it should GET entity by ID', (done) => {
            // create blog post
            chai.request(server)
                .post('/api/blog/posts')
                .send(testdata.entitySample)
                .end((err, res) => {
                    if(err) console.warn(err.response.res.text);

                    let postData = res.body;
                    console.log("here");
                    console.log(postData);

                    // get by id
                    chai.request(server)
                        .get('/api/blog/posts/' + postData._id)
                        .end((err, res) => {
                            if(err) console.warn(err.response.res.text);
                            res.should.have.status(HttpStatus.OK);
                            console.log(res.body);
                            res.body.should.be.deep.equal(postData);
                            done();
                        });

                });
        });
    });

    /*
     * Test the /DELETE route
     */
    describe('/DELETE requests', () => {
        it('it should delete an entity', (done) => {
            chai.request(server)
                .post('/api/blog/posts')
                .send(testdata.entitySample)
                .end((err, res) => {
                    if(err) console.warn(err.response.res.text);

                    let postData = res.body;
                    console.log(postData);

                    chai.request(server)
                        .delete('/api/blog/posts/' + postData._id)
                        .end((err, res) => {
                            if(err) console.warn(err.response.res.text);
                            res.should.have.status(HttpStatus.OK);
                            done();
                        });
                });
        });


        testdata.invalidIds.forEach((data) => {
            it('it should return invalid ID', (done) => {
                chai.request(server)
                    .delete('/api/blog/posts/' + data)
                    .end((err, res) => {
                        if(err) console.warn(err.response.res.text);
                        res.should.have.status(HttpStatus.BAD_REQUEST);
                        done();
                    });
            });
        });

        it('it should return not found', (done) => {
            chai.request(server)
                .delete('/api/blog/posts/' + testdata.entitySample._id)
                .end((err, res) => {
                    if(err) console.warn(err.response.res.text);
                    res.should.have.status(HttpStatus.NOT_FOUND);
                    done();
                });
        });
    });

});