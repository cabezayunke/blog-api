module.exports = {
    "entitySample": {
      "__v": 0,
      "title": "my title",
      "body": "This is my first blog entry",
      "createdAt": "2017-08-04T17:06:31.274Z",
      "createdBy": {
        "userId": "someUserId",
        "name": "someUserName"
      },
      "_id": "5984a997f3bf94001cb97bec"
    },
    "updatedEntitySample": {
        "__v": 0,
        "title": "my UPDATED title",
        "body": "This is my first UPDATED entry",
        "createdAt": "2017-08-04T17:06:31.274Z",
        "createdBy": {
            "userId": "someUserId",
            "name": "someUserName"
        },
        "_id": "5984a997f3bf94001cb97bec"
    },
    "invalidIds": ["invalidId", 3245345, null],
    "invalidPostData": [
      {
        "body": "missing title",
        "createdBy": {
          "userId": "someUserId",
          "name": "someUserName"
        }
      },
      {
        "title": "missing body",
        "createdBy": {
          "userId": "someUserId",
          "name": "someUserName"
        }
      },
      {
        "title": "333333",
        "body": "missing createdBy"
      },
      {
        "title": 235645267,
        "body": "invalid title",
        "createdBy": {
          "userId": "someUserId",
          "name": "someUserName"
        }
      },
      {
        "title": "",
        "body": "empty title",
        "createdBy": {
          "userId": "someUserId",
          "name": "someUserName"
        }
      },
      {
        "title": "invalid body",
        "body": 56256,
        "createdBy": {
          "userId": "someUserId",
          "name": "someUserName"
        }
      },
      {
        "title": "empty body",
        "body": "",
        "createdBy": {
          "userId": "someUserId",
          "name": "someUserName"
        }
      }
    ],
    "invalidPutData": [
        {
            "title": 235645267,
            "body": "invalid title"
        },
        {
            "title": "",
            "body": "empty title"
        },
        {
            "title": "invalid body",
            "body": 56256
        },
        {
            "title": "empty body",
            "body": ""
        }
    ]
  };